from setuptools import setup, find_packages

setup(name='django_aws_cognito',
      version='0.1',
      description='Django-AWS Cognito Package',
      #url='',
      author='Manuj Rastogi',
      author_email='manuj.rastogi@delhivery.com',
      license='MIT',
      packages=find_packages(),
      install_requires=['boto3', 'requests', 'pyjwt', 'djangorestframework'],
      zip_safe=False)