import jwt
import requests
import boto3
#from backend.models import UserProfile, RelatedProfile
import json
import logging
from django.utils.encoding import smart_text
from django.conf import settings
from django.core.cache import cache
from django.contrib.auth.models import BaseUserManager

JWT_OPTIONS = {'verify_signature': False,
               'verify_exp': True,
               'verify_nbf': True,
               'verify_iat': True,
               'verify_aud': False,
               'require_exp': False,
               'require_iat': False,
               'require_nbf': False
               }

class UserProfileClass(object):
    '''
    '''
    def validate_jwt_token(self, token, check_secret=False):
        '''
        '''
        if check_secret:
            secret = settings.JWT_SECRET_KEY
            payload = jwt.decode(str(token), 
                                 secret, algorithms=['HS256'],options = JWT_OPTIONS)
        else:
            payload = jwt.decode(str(token), 
                                 algorithms=['HS256'],options = JWT_OPTIONS)            
        return payload

    def get_awscognito_userprofile_from_redis(self, key):
        return cache.get(key)
    
    def save_awscognito_userporfile_in_redis(self, key, value):
        '''
        key is self.id and value is user_info
        '''
        cache.set(key,json.dumps(value),
                    #ex=settings.REDIS_EXPIRATION_SECONDS
                              )
    
    def get_userinfo_from_awscognito(self, username):
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        user_info = idp_client.admin_get_user(UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                                Username=username)
        return user_info

    def awscognito_user_login(self, username, password):
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        user = idp_client.admin_initiate_auth(UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                       AuthFlow='ADMIN_NO_SRP_AUTH', 
                                       ClientId=settings.DEFAULT_USER_POOL_APP_ID, 
                                       AuthParameters={'USERNAME':request.data['username'], 
                                                       'PASSWORD':request.data['password']}
                                       )
        return user, True

    def make_password(self, user_id, new_password):
        # change the password using access token in response
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        res = idp_client.change_password(PreviousPassword=request.session['raw_password'],
                                        ProposedPassword=new_password,
                                        AccessToken=user['AuthenticationResult']['AccessToken'])
        if res['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise WrongUsernamePassword
        
        
        
        