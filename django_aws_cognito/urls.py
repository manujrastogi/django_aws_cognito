from django.views.generic.base import TemplateView
from django.conf.urls import include, url
from django_aws_cognito import views

urlpatterns = [
    url(r'^providers/$', views.CognitoProviders.as_view()),
    url(r'^signup/$', views.SignUp.as_view()),
    url(r'^admin_initiate_user/$', views.AdminInitiateAuth.as_view()),
    url(r'^public_provider_login/$', views.PublicProviderLogin.as_view()),
    url(r'^merge_public_provider_identity/$', views.MergePublicProviderIdentity.as_view()),
    url(r'^callback/$', views.DefaultCallbackAPI.as_view()),
]