'''
author : manuj.rastogi@delhivery.com
'''
import jwt
import boto3
from django.contrib.auth.models import AnonymousUser
from django_aws_cognito.models import AWSCognitoUser
from django_aws_cognito.utils import UserProfileClass
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.cache import cache

def validate_jwt(token):
    user_profile = UserProfileClass()
    payload = user_profile.validate_jwt_token(token)
    #request.session['awscognito_user_id'] = payload['sub']
    user_info = user_profile.get_awscognito_userprofile_from_redis(
                                    'user_info-'+str(payload['sub']))
    if not user_info:
        user_info = user_profile.get_userinfo_from_awscognito(payload['cognito:username'])
        session_data = {'jwt_token':token,
                        'username':user_info['Username'],
                        }
        # loop through all user attributes
        for data in user_info['UserAttributes']:
            session_data.update({data['Name']:data['Value']})
        user_profile.save_awscognito_userporfile_in_redis('user_info-'+str(payload['sub']), 
                                                          session_data)
    #make sure add user to request after saving profile to redis
    user = AWSCognitoUser(payload['sub'])
    # implement the aws cognito get user here
    return user

def get_user(request):
    if not hasattr(request, '_cached_user'):
        token = request.COOKIES.get('Bearer', None)
        # if token is None then check authorization headers
        if not token and request.META.get('HTTP_AUTHORIZATION', None):
            header = request.META.get('HTTP_AUTHORIZATION', None).split(' ')[0]
            token = request.META.get('HTTP_AUTHORIZATION', None).split(' ')[1]
            if header in ('Token',):
                # get the corresponding jwt token from redis or database
                pass
        try:
            user = validate_jwt(token) if token else AnonymousUser()
            request._cached_user = user
        except jwt.InvalidTokenError:
            # check if sessionid cookie is there then refresh the
            # jwt Bearer Token
            sessionid = request.COOKIES.get('sessionid', None)
            if sessionid:
                user_id = cache.get('sessionid-'+str(sessionid))
                # save user id against sessionid
                # and save jwt and refresh_token againt user id
                session_data = cache.get('user_info-'+str(user_id))
                # else set here
                # use refresh token
                # check in redis if saved jwt was sent from request
                # then only regenrate new jwt token
                if session_data['jwt_token'] == token:
                    try:
                        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
                        user = idp_client.admin_initiate_auth(UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                                   AuthFlow='REFRESH_TOKEN_AUTH', 
                                                   ClientId=settings.DEFAULT_USER_POOL_APP_ID, 
                                                   AuthParameters={'REFRESH_TOKEN':session_data['refresh_token']}
                                                   )
                        session_data.update(
                                {'jwt_token':user['AuthenticationResult']['IdToken']}).update(
                                {'access_token':user['AuthenticationResult']['AccessToken']})
                        cache.set('user_info-'+str(user_id), session_data)
                        request.session['awscognito_user_info'] = session_data
                    except Exception as e:
                        # refresh token also expired
                        return HttpResponseRedirect(settings.LOGOUT_URL)
            return HttpResponseRedirect(settings.LOGOUT_URL)
    return request._cached_user


class AuthenticationMiddleware(object):
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "The Django authentication middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE_CLASSES setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'django_aws_cognito.middlewares.authentication.AuthenticationMiddleware'."
        )
        # check for if login url then add Anonymous user
        request.user = get_user(request)

    def process_response(self, request, response):
        # drop the new jwt here
        # also set token and user id against the sessionid in response
        # here
        if request.session.has_key('awscognito_user_id'):
            user_id = cache.get('sessionid-'+str(request.session.session_key))
            if not user_id:
                # get and set user_id
                user_id = request.session['awscognito_user_id']
                cache.set('sessionid-'+str(request.session.session_key), user_id)
                # set jwt data and user info
                cache.set('user_info-'+str(user_id), 
                          request.session['awscognito_user_info'])
        return response