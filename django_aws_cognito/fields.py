from django_aws_cognito.models import AWSCognitoUser
from django.db import models

class AWSCognitoUserField(models.Field):
    description = "A AWSCognitoUser Custom Field"
    __metaclass__ = models.SubfieldBase
    
    def __init__(self,help_text=("A awscognito user custom field"),verbose_name='awscognitouserfield', *args,**kwargs):
        self.name="AWSCognitoUserField",
        self.through = None
        self.help_text = help_text
        self.blank = True
        self.editable = True
        self.creates_table = False
        self.db_column = None
        self.serialize = False
        self.null = True
        self.creation_counter = models.Field.creation_counter
        models.Field.creation_counter += 1
        super(AWSCognitoUserField, self).__init__(*args, **kwargs)
        
    def db_type(self, connection):
        return 'varchar(200)'
    
    def to_python(self,value):
        '''
        called when database returns value to the 
        field to wrap into specified class
        '''
        if value in ( None,''):
            return AWSCognitoUser()
        else:
            if isinstance(value, AWSCognitoUser):
                return value
            else:
                #raise ValidationError("Invalid input for a GeoCoordinate instance")
                return AWSCognitoUser(value)

    def get_prep_value(self, value):
        return value.id
    
    def get_internal_type(self):
        return 'CharField'
    
    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_prep_value(value)
    
