import requests
import boto3
import botocore
import json
import jwt
from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import hashers #make_password, check_password
from django.contrib.auth.backends import ModelBackend
from django_aws_cognito.utils import UserProfileClass

class WrongUsernamePassword(Exception):
    pass

class CognitoProviders(APIView):
    '''
    List of Login Providers
    '''
    def get(self, request, *ars, **kwargs):
        '''
        '''
        #request.session['providers'] = settings.COGNITO_PROVIDERS
        return Response(data={'providers':settings.COGNITO_PROVIDERS}, status=status.HTTP_200_OK)


class SignUp(APIView):
    '''
    Signup
    '''
    def post(self, request, *ars, **kwargs):
        '''
        '''
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        # check if user already exists
        try:
            check_user = idp_client.admin_get_user(
                            UserPoolId=settings.DEFAULT_USER_POOL_ID,
                            Username=request.data['username'])
            if check_user.get('Username'):
                return Response(data={'user':check_user,
                                  'res':'Already exists'}, status=status.HTTP_400_BAD_REQUEST)
        except botocore.exceptions.ClientError:
            pass
        # get extra information also for user_metadata and app_metadata
        user = idp_client.sign_up(ClientId=settings.DEFAULT_USER_POOL_APP_ID,
                            Username=request.data['username'],
                            Password=request.data['password'],
                            UserAttributes=request.data.get('user_attributes',[]))
        # confirm users by admin
        if settings.AWSCOGNITO_ADMIN_CONFIRM_USER:
            confirm_user_sign_up = idp_client.admin_confirm_sign_up(
                                UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                Username=request.data['username']
                                )
        return Response(data={'user':user}, status=status.HTTP_201_CREATED)


class AdminInitiateAuth(APIView):
    '''
    Login
    '''
    def post(self, request, *args, **kwargs):
        '''
        '''
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        ci_client = boto3.client('cognito-identity', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        # check is user needs a change password
#         change_password_check = False #initialte with False
#         if request.data.get('domain') == 'hq': #check the flag here
#             # request hq api to check password hash and return encoded password
#             encoded, change_password_check = 'Opensource21', True
#         if change_password_check:
#             previous_password = encoded[::10]
#             user = idp_client.admin_initiate_auth(UserPoolId=settings.DEFAULT_USER_POOL_ID,
#                                            AuthFlow='ADMIN_NO_SRP_AUTH', 
#                                            ClientId=settings.DEFAULT_USER_POOL_APP_ID, 
#                                            AuthParameters={'USERNAME':request.data['username'], 
#                                                            'PASSWORD':previous_password}
#                                            )           
#             # change the password using access token in response
#             res = idp_client.change_password(PreviousPassword=previous_password,
#                                             ProposedPassword=request.data['password'],
#                                             AccessToken=user['AuthenticationResult']['AccessToken'])
#             if res['ResponseMetadata']['HTTPStatusCode'] != 200:
#                 raise WrongUsernamePassword
        user = idp_client.admin_initiate_auth(UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                       AuthFlow='ADMIN_NO_SRP_AUTH', 
                                       ClientId=settings.DEFAULT_USER_POOL_APP_ID, 
                                       AuthParameters={'USERNAME':request.data['username'], 
                                                       'PASSWORD':request.data['password']}
                                       )
        user_info = idp_client.admin_get_user(UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                                Username=request.data['username'])
        # setup data in session for redis
        session_data = {'refresh_token':user['AuthenticationResult']['RefreshToken'],
                        'jwt_token':user['AuthenticationResult']['IdToken'],
                        'access_token':user['AuthenticationResult']['AccessToken'],
                        'username':user_info['Username'],
                        }
        # loop through all user attributes
        for data in user_info['UserAttributes']:
            session_data.update({data['Name']:data['Value']})
        request.session['awscognito_user_info'] = session_data             
        user_profile = UserProfileClass()
        payload = user_profile.validate_jwt_token(user['AuthenticationResult']['IdToken'])
        request.session['awscognito_user_id'] = payload['sub']
        # create federated identity
        res = ci_client.get_id(AccountId=settings.ACCOUNTID,
                                IdentityPoolId=settings.IDENTITYPOOLID,
                                Logins={
                                        settings.DEFAULT_USER_POOL_LOGIN_PROVIDER:user['AuthenticationResult']['IdToken']
                                        }
                                )
        return Response(data={'user':user,
                              'user_info':user_info,
                              'res':res}, status=status.HTTP_201_CREATED)


class PublicProviderLogin(APIView):
    '''
    Login Through Public Providers like G+, facebook
    '''
    def post(self, request, *args, **kwargs):
        '''
        '''
        token = request.data['token']
        provider = request.data['provider']
        idp_client = boto3.client('cognito-idp', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        ci_client = boto3.client('cognito-identity', **settings.AWS_COGNITO_DEFAULT_CONFIG)
        try:
            # check is secret is provided to decode securely
            user_profile = UserProfileClass()
            payload = user_profile.validate_jwt_token(token)
        except jwt.InvalidTokenError:
            return Response(data={"error":"Invalid Token"}, 
                        status=status.HTTP_400_BAD_REQUEST)
        # check the user if it exists in USER POOL
        try:
            check_user = idp_client.admin_get_user(
                                                    UserPoolId=settings.DEFAULT_USER_POOL_ID,
                                                    Username=payload['email'])
            if check_user.get('Username'):
                return Response(data={'user':check_user,
                                      'res':'Already exists'}, status=status.HTTP_400_BAD_REQUEST)        
        except botocore.exceptions.ClientError:
            pass
        # get identity id
        res = ci_client.get_id(AccountId=settings.ACCOUNTID,
                                IdentityPoolId=settings.IDENTITYPOOLID,
                                Logins={
                                        provider:token
                                        }
                                )
        return Response(data={'res':res}, status=status.HTTP_201_CREATED)


class MergePublicProviderIdentity(APIView):
    '''
    Implicit linking of the existing accounts over email
    '''
    def post(self, request, *args, **kwargs):
        '''
        '''
        res = ci_client.get_id(AccountId=settings.ACCOUNTID,
                                IdentityPoolId=settings.IDENTITYPOOLID,
                                Logins=request.data
                                )
        return Response(data={'res':res}, status=status.HTTP_201_CREATED)


class DefaultCallbackAPI(APIView):
    '''
    Creating a default callback url for all
    users to provide to public provider so
    everything remains in flow
    '''
    def get(self, request, *args, **kwargs):
        '''
        '''
        res = request.data
        r = Response(data={'res':res}, status=status.HTTP_200_OK)
        return r
