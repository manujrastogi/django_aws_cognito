import json
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, \
                        PermissionsMixin, BaseUserManager
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import send_mail
from django.db import connections
from django.db.models.query import QuerySet
from django.contrib.auth.models import Group, Permission

class AWSCognitoUserManager(BaseUserManager):
    '''
    '''
    def __init__(self, awscognito=None, group_query=False, user_perm_query=False, 
                 _result_cache=None, *args, **kwargs):
        super(AWSCognitoUserManager, self).__init__( *args, **kwargs)
        self.awscognito = awscognito
        self.group_query = group_query
        self.user_perm_query = user_perm_query
        self._result_cache = _result_cache
    
    def __repr__(self):
        if not self._result_cache:
            return self
        data = list(self._result_cache[:REPR_OUTPUT_SIZE + 1])
        if len(data) > REPR_OUTPUT_SIZE:
            data[-1] = "...(remaining elements truncated)..."
        return repr(data)

    def __str__(self):
        return str(self.__class__)

    def __len__(self, *args, **kwargs):
        #self._fetch_all()
        return len(self._result_cache)
        
    def __iter__(self, *args, **kwargs):
        #self._fetch_all()
        return iter(self._result_cache)

    def _fetch_all(self, *args, **kwargs):
        raise NotImplementedError()

    def iterator(self, *args, **kwargs):
        raise NotImplementedError()

    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        raise NotImplementedError()
#         now = timezone.now()
#         if not username:
#             raise ValueError('The given username must be set')
#         email = self.normalize_email(email)
#         user = self.model(username=username, email=email,
#                           is_staff=is_staff, is_active=True,
#                           is_superuser=is_superuser, last_login=now,
#                           date_joined=now, **extra_fields)
#         user.set_password(password)
#         user.save(using=self._db)
#         return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)

    def get_queryset(self):
        return QuerySet(self.model, using=self._db)

    def filter(self, *args, **kwargs):
        if self._result_cache:
            # appy filter on existing data in self._result_cache
            raise NotImplementedError()
        if not self.awscognito._perm_cache:
            self.awscognito.set_user_perms_from_database()
        if self.user_perm_query:
            # check if user perm also gives group perms added into it
            user_perms_objs = Permission.objects.filter(**kwargs)
            sql = '''
            select permission_id from auth_user_user_permissions where permission_id in ({}) and user_id='{}';
            '''.format(','.join(map(str, user_perms_objs.values_list('id', flat=True))), 
                       self.auth0.id)
            rows = self.awscognito.raw_connection(sql.strip())
            user_perms_objs = Permission.objects.filter(id__in=list(set(map(lambda x:x[0], rows))))
            return user_perms_objs
        if self.group_query:
            grp_perms_objs = Group.objects.filter(**kwargs)
            sql = '''
            select group_id from auth_user_groups where group_id in ({}) and user_id='{}';
            '''.format(','.join(map(str, grp_perms_objs.values_list('id', flat=True))), 
                       self.auth0.id)
            rows = self.awscognito.raw_connection(sql.strip())
            grp_perms_objs = Group.objects.filter(id__in=list(set(map(lambda x:x[0], rows))))
            return AWSCognitoUserManager(_result_cache=grp_perms_objs)
            #return grp_perms_objs
    
    def all(self , *args, **kwargs):
        if self._result_cache:
            # appy filter on existing data in self._result_cache
            raise NotImplementedError()
        if not self.awscognito._perm_cache:
            self.awscognito.set_user_perms_from_database()
        if self.user_perm_query:
            # check if user perm also gives group perms added into it
            user_perms_objs = Permission.objects.filter(**kwargs)
            sql = '''
            select permission_id from auth_user_user_permissions where user_id='{}';
            '''.format(self.auth0.id)
            rows = self.awscognito.raw_connection(sql.strip())
            user_perms_objs = Permission.objects.filter(id__in=list(set(map(lambda x:x[0], rows))))
            return user_perms_objs
        if self.group_query:
            grp_perms_objs = Group.objects.filter(**kwargs)
            sql = '''
            select group_id from auth_user_groups where user_id='{}';
            '''.format(self.awscognito.id)
            rows = self.awscognito.raw_connection(sql.strip())
            grp_perms_objs = Group.objects.filter(id__in=list(set(map(lambda x:x[0], rows))))
            return AWSCognitoUserManager(_result_cache=grp_perms_objs)
        raise NotImplementedError()

    def exists(self):
        if self._result_cache is None:
            return False
        return bool(self._result_cache)

    def count(self):
        if self._result_cache is not None:
            return len(self._result_cache)
        #return self.query.get_count(using=self.db)
        raise NotImplementedError()
    
    
