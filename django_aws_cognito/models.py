import json
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, \
                        PermissionsMixin, BaseUserManager
#from backend.models import UserProfile
from django_aws_cognito.utils import UserProfileClass as ExtendedAWSCognitoUserProfile
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import send_mail
from django.db import connections
from django.db.models.query import QuerySet
from django.contrib.auth.models import Group, Permission
from django_aws_cognito.managers import AWSCognitoUserManager
#from django.db.models.fields.related import ManyRelatedManager
#from urllib import urlquote
REPR_OUTPUT_SIZE = 20

class SiteProfileNotAvailable(Exception):
    pass

class AWSCognitoUser(AbstractBaseUser):#, PermissionsMixin):
    '''
    '''
    pk,id = None,None
    username = None
    email = None
    is_staff = False
    is_active = True
    first_name = None
    last_name = None
    awscognito_profile = None
    user_info = None
    _perm_cache = None
    _group_perm_cache = None
    is_superuser = False
    #date_joined = None
    objects = AWSCognitoUserManager()

    class Meta:
        abstract = True
    #USERNAME_FIELD = 'auth_username'
    
    def __init__(self, user_id, *args, **kwargs):
        super(AWSCognitoUser, self).__init__(*args, **kwargs)
        self.id = user_id
        self.pk = user_id
        self.awscognito_profile = ExtendedAWSCognitoUserProfile()
        # get data from redis
        self.user_info = json.loads(self.awscognito_profile.get_awscognito_userprofile_from_redis(
                                                'user_info-'+str(self.id)))
        self.username = self.user_info['username'] if self.user_info('username',None) \
                                                    else self.user_info['email']
        self.email = self.user_info['email'] if self.user_info('email',None) \
                                                else self.user_info['username']
        self.first_name = self.user_info.get('first_name', '')
        self.last_name = self.user_info.get('last_name', '')
        self.is_staff = True if self.user_info.get('is_staff', False) else False  
        self.is_active = True if self.user_info.get('is_active', False) else False
        self.is_superuser = True if self.user_info.get('is_superuser', False) else False

    def __unicode__(self):
        return self.username
    
    @property
    def groups(self):
        '''
        user.groups.filter(name='Finance Executive').exists()
        '''
        #return ManyRelatedManager()
        obj = AWSCognitoUserManager(self, group_query=True)
        return obj
    
    @property
    def user_permissions(self):
        obj = AWSCognitoUserManager(self, user_perm_query=True)
        return obj
    
    def get_username(self):
        return self.username
    
    def is_anonymous(self):
        return False
    
    def __str__(self):
        return self.get_username()

    def natural_key(self):
        return (self.get_username(),)
    
    def save(self):
        raise NotImplementedError()
    
    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def get_profile(self):
        """
        Returns site-specific profile for this user. Raises
        SiteProfileNotAvailable if this site does not allow profiles.
        """
        return self.userprofile
        #raise SiteProfileNotAvailable
    
    def raw_connection(self, sql):
        # change connection as per read or write
        cursor = connections['default'].cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        cursor.close()
        return rows
    
    def set_user_perms_from_database(self):
        self.id = 100
        sql = '''
            select ct.app_label, ap.codename from auth_permission ap, django_content_type ct 
            where ct.id=ap.content_type_id and ap.id in (
            select permission_id from auth_user_user_permissions where user_id='{}');
            '''.strip()
        perms_data = self.raw_connection(sql.format(str(self.id)))
        self._perm_cache = set(["%s.%s" % (p[0], p[1]) for p in perms_data])
        sql = '''
            select ct.app_label, ap.codename from auth_permission ap, django_content_type ct 
            where ct.id=ap.content_type_id and ap.id in (
            select agp.permission_id from auth_group_permissions agp, 
            auth_user_groups aug where aug.user_id='{}' and aug.group_id=agp.group_id);
            '''.strip()
        grp_perms_data = self.raw_connection(sql.format(str(self.id)))
        self._group_perm_cache = set(["%s.%s" % (p[0], p[1]) for p in grp_perms_data])
        self._perm_cache.update(self._group_perm_cache)

    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through their
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        if not self._group_perm_cache:
            self.set_user_perms_from_database() 
        return self._group_perm_cache

    def get_all_permissions(self, obj=None):
        if not self._perm_cache:
            self.set_user_perms_from_database() 
        return self._perm_cache

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """
        return True
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True
        if not self._perm_cache:
            self.set_user_perms_from_database()
        if perm in self._perm_cache:
            return True
        return False

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        return True
        if not self._perm_cache:
            self.set_user_perms_from_database()
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        return True
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True
        if not user_obj.is_active:
            return False
        if not self._perm_cache:
            self.set_user_perms_from_database()
        for perm in self._perm_cache:
            if perm[:perm.index('.')] == app_label:
                return True
        return False

    def is_authenticated(self):
        if self.id:
            return True
        return False
    
    def set_password(self, raw_password):
        '''
        set new password for user in auth0
        '''
        self.awscognito_profile.make_password(self.id, raw_password)
        # set the raw+password field to null
        request.session['raw_password'] = ''

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        data, flag = self.awscognito_profile.awscognito_user_login(self.username, 
                                                         raw_password)
        # delete this password upon call on make_password
        request.session['raw_password'] = raw_password
        return flag

    #@property
    #def userprofile(self):
        #profile = UserProfile.objects.get(user = self.id)
        #self.__setattr__('is_staff', self.is_staff)
        #profile = UserProfile.objects.all()[0]
        #self.__setattr__('is_staff', True)
        #return profile

